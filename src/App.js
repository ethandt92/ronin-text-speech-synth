import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import Slider from 'material-ui/Slider';
import Paper from 'material-ui/Paper';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import CircularProgress from 'material-ui/CircularProgress'
import firebase from 'firebase'
import uniqBy from 'lodash/uniqBy'
import throttle from 'lodash/throttle'
import chunk from 'lodash/chunk'
import flatMapDepth from 'lodash/flatMapDepth'

injectTapEventPlugin()

// const _LTracker = _LTracker || []

const LIMIT = 10

// const SYNTH_TYPE = 'responsiveVoice'
// const SYNTH_TYPE = 'native'
// const SYNTH_TYPE = 'watsonSpeech'
const SYNTH_TYPES = ['responsiveVoice', 'native', 'watsonSpeech']

const FB_PROJECT_ID = "ronin-7fe02"
const config = {
  // apiKey: "<API_KEY>",
  // authDomain: "<PROJECT_ID>.firebaseapp.com",
  databaseURL: "https://" + FB_PROJECT_ID + ".firebaseio.com",
  storageBucket: 'gs://' + FB_PROJECT_ID + ".appspot.com",
}

firebase.initializeApp(config)

const storage = firebase.storage()
const storage_ref = storage.ref()

const WatsonSpeech = {
  TextToSpeech: {
    getVoices: require("watson-speech/text-to-speech/get-voices"),
    synthesize: require("watson-speech/text-to-speech/synthesize")
  }
}

const API_ENDPOINT = 'https://text-speech-synth.herokuapp.com'
// const API_ENDPOINT = 'https://ronin.ngrok.io'

function log(obj) {
  // console.log(obj)
  // obj = JSON.stringify(obj)
  // _LTracker.push(obj)
  for (var i in obj){
    console.log(obj[i], i);
  }
}

async function fetchWatsonSpeechAuthToken() {
  try {
    const response = await fetch(API_ENDPOINT+'/api/text-to-speech/token')
    return response.text()
  } catch (error) {
    console.error(error)
  }
}

async function wsGetVoices() {
  try {
    const token = await fetchWatsonSpeechAuthToken()
    const voices = await WatsonSpeech.TextToSpeech.getVoices({token})
    return voices
  } catch (error) {
    console.error(error)
  }
}

async function wsSynthesize(options) {
  try {
    const token = await fetchWatsonSpeechAuthToken()
    const audio = await WatsonSpeech.TextToSpeech.synthesize({
      token,
      ...options
    })
    return audio
  } catch (error) {
    console.error(error)
  }
}

const iconButtonElement = (
  <IconButton
    touch={true}
    tooltip="more"
    tooltipPosition="bottom-left"
  >
    <MoreVertIcon color={grey400} />
  </IconButton>
)

const rightIconMenu = (
  <IconMenu iconButtonElement={iconButtonElement}>
    <MenuItem>Reply</MenuItem>
    <MenuItem>Forward</MenuItem>
    <MenuItem>Delete</MenuItem>
  </IconMenu>
)

const styles = {
  selected: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  },
  displayText: {
    height: 'auto',
    padding: '10px 24px'
  },
  displayTextTitle: {
    lineHeight: '20px',
    fontSize: '15px',
    whiteSpace: 'normal',
    overflow: 'scroll',
    height: '15vh',
  },
  loadButton: {
    flex: 1
  },
  toolbarButtonFirst: {
    margin: '10px 10px 10px 10px'
  },
  toolbarButton: {
    margin: '10px 10px 10px 0'
  },
  toolbarGroup: {
    marginLeft: '-10px'
  },
  slider: {
    margin: 0
  },
  sliderLabel: {
    fontSize: '14px'
  },
  listItem: {

  }
}

class PlayList extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.items !== this.props.items)
      return true
    if (nextProps.selected !== this.props.selected)
      return true
    return false
  }
  render() {
    const {items, renderRow, ...otherProps} = this.props
    return (
      <List className="App-list" {...otherProps}>
        {items.map(renderRow)}
      </List>
    )
  }
}

const IMSDBListItem = ({selected, item, ...otherProps}) => (
  <ListItem
    {...otherProps}
    style={Object.assign({},
      styles.listItem,
      selected === item.slug && styles.selected
    )}
    key={item.slug}
    leftAvatar={<Avatar src={item.image} />}
    primaryText={item.title}
  />
)

const MediumListItem = ({selected, item, ...otherProps}) => {
  return (
  <ListItem
    {...otherProps}
    style={Object.assign({},
      styles.listItem,
      selected === item.id && styles.selected
    )}
    key={item.id}
    leftAvatar={<Avatar src={item.image || item.author_image} />}
    primaryText={item.title}
    secondaryText={item.description}
  />
)}

const GeniusListItem = ({selected, item, ...otherProps}) => (
  <ListItem
    {...otherProps}
    style={Object.assign({},
      styles.listItem,
      selected === item.id && styles.selected
    )}
    key={item.id}
    leftAvatar={<Avatar src={item.image} />}
    primaryText={item.title}
    secondaryText={item.description}
  />
)

class SynthTypeSelectField extends Component {
  render() {
    const {synthTypes, ...otherProps} = this.props
    if (!synthTypes || synthTypes.length === 0) 
      return null
    return (
      <SelectField fullWidth={true} floatingLabelText="Synth Type" {...otherProps}>
        {synthTypes.map((e, i) => (
          <MenuItem key={e} value={e} primaryText={e} />
        ))}
      </SelectField>
    )
  }
}

class VoiceSelectField extends Component {
  render() {
    const {voices, ...otherProps} = this.props
    if (!voices || voices.length === 0) 
      return null
    return (
      <SelectField fullWidth={true} floatingLabelText="Voice" {...otherProps}>
        {voices.map((e, i) => (
          <MenuItem key={e.name} value={e} primaryText={e.name} />
        ))}
      </SelectField>
    )
  }
}

class WebsiteSelectField extends Component {
  render() {
    return (
      <SelectField fullWidth={true} floatingLabelText="Website" {...this.props}>
        {WEBSITES.map((e, i) => (
          <MenuItem key={e.name} value={e} primaryText={e.name} />
        ))}
      </SelectField>
    )
  }
}

const WEBSITES = [
  {
    key: 'imsdb', 
    name:'IMSDB',
    uniqKey: 'slug',
    ListItem: IMSDBListItem
  },
  {
    key: 'medium', 
    name:'Medium',
    uniqKey: 'id',
    ListItem: MediumListItem
  },
  {
    key: 'genius', 
    name:'Genius',
    uniqKey: 'id',
    ListItem: GeniusListItem
  }
]

function createMarkup(__html) { 
  return {__html}; 
};

function getEndWordIndex(str, pos) {

    // Perform type conversions.
    str = String(str);
    pos = Number(pos) >>> 0;

    // Search for the word's beginning and end.
    const left = str.slice(0, pos + 1).search(/\S+$/),
        right = str.slice(pos).search(/\s/);

    if (right < 0)
      return left

    return right + pos
}

let _prevAudio = null
let _currAudio = null
let _listener = null
let _audioCache = {}
let _textCache = {}
let _cacheIndex = 0
let _customTextCache = null
let _customAudioCache = null
let _customCacheIndex = 0

function audioProps(audio) {
  const {
    audioTracks,
    autoplay,
    buffered,
    controller,
    controls,
    crossOrigin,
    currentSrc,
    currentTime,
    defaultMuted,
    defaultPlaybackRate,
    duration,
    ended,
    error,
    loop,
    mediaGroup,
    muted,
    networkState,
    paused,
    playbackRate,
    played,
    preload,
    readyState,
    seekable,
    seeking,
    src,
    textTracks,
    volume
  } = audio
  return {
    audioTracks,
    autoplay,
    buffered,
    controller,
    controls,
    crossOrigin,
    currentSrc,
    currentTime,
    defaultMuted,
    defaultPlaybackRate,
    duration,
    ended,
    error,
    loop,
    mediaGroup,
    muted,
    networkState,
    paused,
    playbackRate,
    played,
    preload,
    readyState,
    seekable,
    seeking,
    src,
    textTracks,
    volume
  }
}

function removeEmojis(str) {
  return str.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '')
}

function removeControls(str) {
  return str.replace(/[^\x20-\x7E]/gmi, " ")
}

function cleanSpaces(str) {
  return str.replace(/ +(?= )/g,'')
}

function removeEmojiAndOthers(str) {
  return str.replace(/[^\w.,\s\u0104\u0106\u0118\u0141\u0143\u00D3\u015A\u0179\u017B\u0105\u0107\u0119\u0142\u0144\u00F3\u015B\u017A\u017C]/gu, '')
}

function cleanText(str) {
  return cleanSpaces(removeEmojis(str))
}

function textToWords(str) {
  return str.split(" ")
}

function wordsToText(arr) {
  return Array.isArray(arr) ? arr.join(" ") : arr
}

function chunkText(text) {
  const words = textToWords(text)
  const chunkedWords = chunk(words, 100)
  const chunkedText = flatMapDepth(chunkedWords, wordsToText, 2)
  return chunkedText
}

async function synthesize(text, voice, ...args) {
  try {
    const opts = (args && args[0]) || {}
    return await wsSynthesize({text, voice, autoPlay: false, ...opts})
  } catch (error) {
    console.error(error)
  }
}

function prepend(a, b) {
  if (Array.isArray(b))
    return [...b, ...a]
  return [b, ...a]
}

function append(a, b) {
  if (Array.isArray(b))
    return [...a, ...b]
  return [...a, b]
}

function snapValue(snap) {
  return {
    key: snap.key, 
    ...snap.val()
  }
}

function snapToArray(snap) {
  let items = []
  snap.forEach((childSnap) => {
    const item = childSnap.val()
    items.push(item)
  });
  return items
}

async function fetchStorageText(path, id) {
  const url = await storage_ref
    .child(path + '_text')
    .child(id)
    .getDownloadURL()
  const headers = new Headers();
  const request = new Request(url, {headers});
  const response = await fetch(request)
  return response ? response.text() : 'No text found'
}

async function fetchOnce(path, {orderByChild, startAt, limitToLast}) {
  const ref = firebase.database()
    .ref(path)
    .orderByChild(orderByChild)
    .endAt(startAt)
    .limitToLast(limitToLast)
  const snap = await ref.once('value')
  return snapToArray(snap)
}

async function fetchMore(path, startAt) {
  let items = await fetchOnce(path, {
    orderByChild: 'updated_at',
    startAt,
    limitToLast: LIMIT
  })
  items.reverse()
  items.shift()
  return items
}

class App extends Component {
  constructor(props) {
    super(props);
    this.initialState = {
      value: '',
      items: [],
      voices: [],
      charIndex: 0,
      displayText: '',
      queue: [],
      queueIndex: 0,
      queueWebsite: null,
      progressDragging: false,
      progressChangeValue: 0,
      progressValue: 0,
      progressMax: 1,
      progressAdjustedValue: 0,
      progressPercent: 0,
      website: null,
      synthType: SYNTH_TYPES[0],
      selectedQueueItemId: null
    }
    this.state = {...this.initialState}
  }
  componentDidMount() {
    this.resetSynth()
    this.listenToEvents()
    this.setState(this.getSynthState())
    const utterance = new SpeechSynthesisUtterance()
    utterance.onboundary = this.onboundary
    utterance.onend = this.onend
    utterance.onerror = this.onerror
    utterance.onmark = this.onmark
    utterance.onpause = this.onpause
    utterance.onresume = this.onresume
    utterance.onstart = this.onstart
    this.utterance = utterance
  }
  componentWillUnmount() {
    this.resetSynth()
  }
  getSynth = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice') 
      return window.responsiveVoice
    else if (synthType === 'watsonSpeech') 
      return WatsonSpeech.TextToSpeech
    return window.speechSynthesis
  }
  voiceSupport = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice')
      return this.getSynth().voiceSupport()
    else if (synthType === 'native')
      return !!this.getSynth()
    return true
  }
  getDefaultVoice = () => {
    if (this.getSynthType() === 'watsonSpeech')
      return {
        "name": "en-US_MichaelVoice",
        "language": "en-US",
        "customizable": true,
        "gender": "male",
        "url": "https://stream.watsonplatform.net/text-to-speech/api/v1/voices/en-US_MichaelVoice",
        "description": "Michael: American English male voice."
     }
    return this.getSynth().getVoices()[0]
  }
  listenToEvents = (audio) => {
    const synthType = this.getSynthType()
    if (synthType === 'native') {
      let synth = this.getSynth()
      synth.onboundary = this.onboundary
    } else if (synthType === 'responsiveVoice') {
      let synth = this.getSynth()
      synth.onboundary = this.onboundary
      synth.onPartEnd = this.onPartEnd
      synth.speech_onend = this.speech_onend
      synth.speech_onstart = this.speech_onstart
      synth.AddEventListener("OnLoad", this.onload)
      synth.OnVoiceReady = this.onready
    } else if (synthType === 'watsonSpeech' && audio) {
      // audio.addEventListener('abort', this.onabort)
      // audio.addEventListener('canplay', this.oncanplay)
      // audio.addEventListener('canplaythrough', this.oncanplaythrough)
      audio.addEventListener('durationchange', this.ondurationchange)
      // audio.addEventListener('emptied', this.onemptied)
      audio.addEventListener('ended', this.onended)
      audio.addEventListener('error', this.onerror)
      // audio.addEventListener('loadeddata', this.onloadeddata)
      // audio.addEventListener('loadedmetadata', this.onloadedmetadata)
      audio.addEventListener('loadstart', this.onloadstart)
      audio.addEventListener('pause', this.onpause)
      audio.addEventListener('play', this.onplay)
      audio.addEventListener('playing', this.onplaying)
      // audio.addEventListener('progress', this.onprogress)
      // audio.addEventListener('ratechange', this.onratechange)
      // audio.addEventListener('seeked', this.onseeked)
      // audio.addEventListener('seeking', this.onseeking)
      // audio.addEventListener('stalled', this.onstalled)
      // audio.addEventListener('suspend', this.onsuspend)
      audio.addEventListener('timeupdate', this.ontimeupdate)
      // audio.addEventListener('volumechange', this.onvolumechange)
      // audio.addEventListener('waiting', this.onwaiting)
    }
  }
  unlistenToEvents = (audio) => {
    if (audio) {
      // audio.removeEventListener('abort', this.onabort)
      // audio.removeEventListener('canplay', this.oncanplay)
      // audio.removeEventListener('canplaythrough', this.oncanplaythrough)
      audio.removeEventListener('durationchange', this.ondurationchange)
      // audio.removeEventListener('emptied', this.onemptied)
      audio.removeEventListener('ended', this.onended)
      audio.removeEventListener('error', this.onerror)
      // audio.removeEventListener('loadeddata', this.onloadeddata)
      // audio.removeEventListener('loadedmetadata', this.onloadedmetadata)
      audio.removeEventListener('loadstart', this.onloadstart)
      audio.removeEventListener('pause', this.onpause)
      audio.removeEventListener('play', this.onplay)
      audio.removeEventListener('playing', this.onplaying)
      // audio.removeEventListener('progress', this.onprogress)
      // audio.removeEventListener('ratechange', this.onratechange)
      // audio.removeEventListener('seeked', this.onseeked)
      // audio.removeEventListener('seeking', this.onseeking)
      // audio.removeEventListener('stalled', this.onstalled)
      // audio.removeEventListener('suspend', this.onsuspend)
      audio.removeEventListener('timeupdate', this.ontimeupdate)
      // audio.removeEventListener('volumechange', this.onvolumechange)
      // audio.removeEventListener('waiting', this.onwaiting)
    }
  }
  synthCancel = () => {
    if (this.getSynthType() === 'watsonSpeech') {
      this.unlistenToEvents(_prevAudio)
      this.unlistenToEvents(_currAudio)
      _prevAudio && _prevAudio.pause()
      _currAudio && _currAudio.pause()
      _prevAudio = null
      _currAudio = null
      _cacheIndex = 0
      _customTextCache = null
      _customAudioCache = null
      _customCacheIndex = 0
    } else
      this.getSynth().cancel()
  }
  synthSpeak = async (text, ...args) => {
    const synthType = this.getSynthType()
    try {
      if (synthType === 'watsonSpeech') {
        const audio = await synthesize(text, this.getVoice().name, ...args)
        this.playAudio(audio)
        return audio
      } else if (synthType === 'native')
        this.getSynth().speak(text)
      else
        this.getSynth().speak(text, this.getVoice().name, ...args)
    } catch (error) {
      console.error(error)
    }
  }
  synthPause = (audio) => {
    if (this.getSynthType() === 'watsonSpeech')
      audio.pause()
    else
      this.getSynth().pause()
  }
  synthResume = (audio) => {
    if (this.getSynthType() === 'watsonSpeech')
      this.playAudio(audio)  
    else
      this.getSynth().resume()
  }
  playAudio = (audio) => {
    this.unlistenToEvents(_currAudio)
    _currAudio && _currAudio.pause()
    _prevAudio = _currAudio
    _currAudio = audio
    this.listenToEvents(audio)
    audio && audio.play()
  }
  resetSynth = () => {
    // if (this.voiceSupport()) {
    //   if (this.getSynthType() === 'responsiveVoice') {
    //     setTimeout(()=>this.getSynth().setDefaultVoice(this.getDefaultVoice().name), 1000)
    //     setTimeout(()=>this.synthCancel(), 1100)
    //     setTimeout(()=>this.synthSpeak(' ', this.getDefaultVoice().name), 1200)
    //   }
    // }
  }
  getSynthState = () => {
    const synthType = this.getSynthType()
    const synth = this.getSynth()
    if (synthType === 'responsiveVoice') {
      const isPlaying = synth.isPlaying()
      return {
        pending: isPlaying,
        speaking: isPlaying,
        loaded: isPlaying,
        cancelled: synth.cancelled
      }
    } else if (synthType === 'watsonSpeech') {
      return {
        paused: _currAudio && _currAudio.paused,
        pending: false,
        speaking: _currAudio && !_currAudio.paused,
        loaded: !!_currAudio,
        ended: _currAudio && _currAudio.ended
      }
    }
    return {
      paused: synth.paused,
      pending: synth.pending,
      speaking: synth.speaking,
      loaded: synth.speaking
    }
  }
  updateProgress = (progressValue, progressMax) => {
    this.setState({progressValue})
    if (isFinite(progressMax)) {
      const progressAdjustedValue = Math.min(progressValue, progressMax)
      const progressPercent = Math.round(progressAdjustedValue / progressMax * 100)
      this.setState({
        progressMax,
        progressAdjustedValue,
        progressPercent
      })
    }
  }
  getQueueItemId = (queueIndex) => {
    const {queue, queueWebsite} = this.state
    const queueItem = queue[queueIndex]
    const queueItemId = queueWebsite && queueItem[queueWebsite.uniqKey]
    return queueItemId
  }
  getCachedTextItems = (id) => {
    return id ? _textCache[id] : _customTextCache
  }
  setCachedTextItems = (id, text) => {
    const chunkedText = chunkText(text)
    if (id)  {
      _textCache[id] = chunkedText
      return _textCache
    }
    _customTextCache = chunkedText
    return _customTextCache
  }
  getCachedTextItem = (id, index) => {
    if (!id && _customTextCache)
      return _customTextCache[index]
    const cachedTextItems = this.getCachedTextItems(id)
    return cachedTextItems ? cachedTextItems[index] : null
  }
  getCachedAudioItems = (id) => {
    return id ? _audioCache[id] : _customAudioCache
  }
  getCachedAudioItem = (id, index) => {
    if (!id && _customAudioCache)
      return _customAudioCache[index]
    const cachedAudioItems = this.getCachedAudioItems(id)
    return cachedAudioItems ? cachedAudioItems[index] : null
  }
  setCachedAudioItem = (id, index, audio) => {
    if (!id) {
      if (!_customAudioCache)
        _customAudioCache = []
      _customAudioCache[index] = audio
      return _customAudioCache
    }
    if (!_audioCache[id])
      _audioCache[id] = []
    _audioCache[id][index] = audio
    return _audioCache
  }
  getSynthType = () => this.state.synthType
  setSynthType = (synthType) => this.setState({synthType}, this.fetchVoices)
  fetchVoices = async () => {
    let voices;
    if (this.getSynthType() === 'watsonSpeech')
      voices = await wsGetVoices()
    else
      voices = this.getSynth().getVoices()
    this.setVoices(voices)
  }
  getVoices = () => this.state.voices
  setVoices = voices => this.setState({voices}, ()=>this.setVoice(this.getDefaultVoice()))
  getPitch = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      return this.state.pitch
    else if (synthType === 'native')
      return this.utterance.pitch
  }
  setPitch = (pitch) => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      this.setState({pitch})
    else if (synthType === 'native')
      this.utterance.pitch = pitch
  }
  getRate = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      return this.state.rate
    else if (synthType === 'native')
      return this.utterance.rate
  }
  setRate = (rate) => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      this.setState({rate})
    else if (synthType === 'native')
      this.utterance.rate = rate
  }
  getVolume = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      return this.state.volume
    else if (synthType === 'native')
      return this.utterance.volume
  }
  setVolume = (volume) => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      this.setState({volume})
    else if (synthType === 'native')
      this.utterance.volume = volume
  }
  getVoice = () => { 
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'watsonSpeech')
      return this.state.voice || this.getDefaultVoice()
    else if (synthType === 'native')
      return this.utterance.voice || this.getDefaultVoice()
  }
  setVoice = (voice) => {
    const synthType = this.getSynthType()
    this.setState({voice})
    if (synthType === 'native')
      this.utterance.voice = voice
  }
  getItems = () => this.state.items
  setItems = (items) => this.setState({items})
  resetItems = () => this.setItems(this.initialState.items)
  loadMore = async () => {
    const {website} = this.state
    // this.setState({loading: true})
    const items = this.getItems()
    const startAt = items[items.length-1].updated_at
    const newItems = await fetchMore(website.key, startAt)
    const appended = append(items, newItems)
    const uniqed = uniqBy(appended, website.uniqKey)
    // this.setState({loading: false})
    this.setItems(uniqed)
  }
  onChildAdded = (snap) => {
    this.setItems(
      prepend(this.getItems(), snapValue(snap))
    )
  }
  speakCustomText = () => {
    const cleanedText = cleanText(this.state.value)
    if (this.getSynthType() === 'watsonSpeech') {
      if (!this.speakCached(null, _customCacheIndex)) {
        this.setCachedTextItems(null, cleanedText)
        if (!this.speakCached(null, _customCacheIndex))
          console.error('error speakCustomText')
      }
    } else {
      this.speak(cleanedText)
    }
  }
  speakCustomText = throttle(this.speakCustomText, 1000)
  resetQueue = (cb) => {
    const {website} = this.state
    this.setState({
      queue: this.getItems(),
      queueWebsite: website,
    }, cb)
  }
  speakItem = (item, queueIndex) => {
    this.resetQueue(()=>this.speakQueueItem(queueIndex, false))
  }
  speakQueueItem = (queueIndex, manualEnd=true) => {
    const {queue} = this.state
    if (!queue || queueIndex < 0 || queueIndex === queue.length || queue.length === 0) 
      return
    const queueItemId = this.getQueueItemId(queueIndex)
    if (this.state.selectedQueueItemId === queueItemId)
      return
    this.setState({queueIndex, selectedQueueItemId: queueItemId}, async ()=>{
      const queueItem = queue[queueIndex]
      if (this.getSynthType() === 'watsonSpeech') {
        if (!this.speakCached(queueItemId, _cacheIndex)) {
          const text = await this.fetchQueueText(queueItem)
          const cleanedText = cleanText(text)
          this.setCachedTextItems(queueItemId, cleanedText)
          if (!this.speakCached(queueItemId, _cacheIndex))
            console.error('error speakQueueItem')
        }
      } else {
        const text = await this.fetchQueueText(queueItem)
        const cleanedText = cleanText(text)
        this.speak(cleanedText, manualEnd)
      }
    })
  }
  speakQueueItem = throttle(this.speakQueueItem, 1000)
  preloadAudioFromTextCache = async (id, index) => {
    const hasCachedAudio = !!this.getCachedAudioItem(id, index)
    const cachedTextItem = this.getCachedTextItem(id, index)
    if (!hasCachedAudio && cachedTextItem) {
      const audio = await synthesize(cachedTextItem, this.getVoice().name)
      this.setCachedAudioItem(id, index, audio)
      audio.preload = "auto"
    }
  }
  speakCached = (id, index) => {
    const cachedAudioItem = this.getCachedAudioItem(id, index)
    const cachedTextItem = this.getCachedTextItem(id, index)
    if (cachedAudioItem) {
      this.playAudio(cachedAudioItem)
      this.preloadAudioFromTextCache(id, index + 1)
      return true
    } else if (cachedTextItem) {
      this.synthSpeak(cachedTextItem, {autoPlay:false})
      this.preloadAudioFromTextCache(id, index + 1)
      return true
    }
    return false
  }
  speak = (text, manualEnd) => {
    this.manualEnd = manualEnd  
    const synthType = this.getSynthType() 
    if (synthType === 'responsiveVoice') {
      setTimeout(()=>this.synthCancel(), 1000) 
      setTimeout(()=>
        this.synthSpeak(text, {
          onboundary: this.onboundary,
          onend: this.onend,
          onerror: this.onerror,
          onmark: this.onmark,
          onpause: this.onpause,
          onresume: this.onresume,
          onstart: this.onstart,
          pitch: this.getPitch(),
          rate: this.getRate(),
          volume: this.getVolume()
        }), 2000)
    } else if (synthType === 'native') {
      this.utterance.text = text
      setTimeout(()=>this.synthCancel(), 1000) 
      setTimeout(()=>this.synthSpeak(this.utterance), 2000)
    } else if (synthType === 'watsonSpeech') {
      this.synthSpeak(text, {autoPlay:false})
    }
  }
  fetchQueueText = async (queueItem) => {
    try {
      const {queueWebsite} = this.state
      const queueItemId = queueItem[queueWebsite.uniqKey]
      return fetchStorageText(queueWebsite.key, queueItemId)
    } catch (error) {
      console.error(error)
    }
    return 'No text found'
  }
  prev = () => this.speakQueueItem(this.state.queueIndex-1)
  next = () => this.speakQueueItem(this.state.queueIndex+1)
  onload = () => this.abort
  onready = (event) => {
    this.fetchVoices()
  }
  onboundary = (event) => {
    // log(event)
    if (this.getSynthType() === 'responsiveVoice') {

    } else {
      const charIndex = event.charIndex
      this.setState({
        charIndex,
        ...this.getSynthState()
      })
      if (!this.state.dragging) {
        this.setState({
          progressChangeValue: charIndex,
        })
        this.updateProgress(charIndex, this.utterance.text.length)
      }
    }
  }
  onend = (event) => {
    // log(event)
    this.setState(this.getSynthState())
    this.setState({ended: true})
    if (this.getSynthType() === 'watsonSpeech') {
      if (_customTextCache) {
        _customCacheIndex += 1 
        if (!this.speakCached(null, _customCacheIndex)) {
          // cancel()
          _customCacheIndex = 0
        }
      } else {
        _cacheIndex += 1
        if (!this.speakCached(this.getQueueItemId(this.state.queueIndex), _cacheIndex)) {
          this.synthCancel()
          this.next()
        }
      }
    } else if (!this.manualEnd) {
      this.next()
    }
    this.manualEnd = false
  }
  speech_onend = this.onend
  onPartEnd = (event) => {
    // log(event)
    const utterances = this.getSynth().utterances
    const utterance = event.target || event.utterance || this.utterance
    if (utterance) {
      const nextUtteranceIndex = utterance.rvIndex + 1
      this.setState(this.getSynthState())
      this.updateProgress(nextUtteranceIndex, utterance.rvTotal)
      if (utterances && nextUtteranceIndex < utterances.length) {
        const nextUtterance = utterances[nextUtteranceIndex]
        this.setState({displayText: nextUtterance.text})
      }
    }
  }
  onerror = (event) => {
    // log(event)
    this.setState(this.getSynthState())
  }
  onmark = (event) => {
    // log(event)
    this.setState(this.getSynthState())
  }
  onpause = (event) => {
    // log(event)
    this.setState({...this.getSynthState(), paused: true})
  }
  onresume = (event) => {
    // log(event)
    this.setState({...this.getSynthState(), paused: false})
  }
  onstart = (event) => {
    // log(event)
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice' || synthType === 'native') {
      const utterance = event.target || event.utterance || this.utterance
      this.setState({
        ...this.getSynthState(), 
        paused: false, 
        displayText: utterance && utterance.text,
        loading: false,
        ended: false
      })
      if (utterance)
        this.updateProgress(0,  utterance.text.length)
    }
  }
  speech_onstart = this.onstart
  onabort = (event) => {
    // log(event)
    // console.log(audioProps())
  }
  oncanplay = this.onabort
  oncanplaythrough = this.onabort
  ondurationchange = (event) => {
    // log(event)
    const audio = event.target && audioProps(event.target)
    if (audio) {
      // this.updateProgress(audio.currentTime, audio.duration)
      // this.setState({loading: audio.networkState === 2})
    }
  }
  onemptied = this.onabort
  onended = this.onend
  onloadeddata = this.onabort
  onloadedmetadata = this.onabort
  onloadstart = (event) => {
    // log(event)
    if (_customTextCache)
      return
    this.setCachedAudioItem(this.getQueueItemId(this.state.queueIndex), _cacheIndex, event.target)
  }
  onplay = (event) => {
    // log(event)
    this.setState(this.getSynthState())
    // this.setState({loading: false})
    if (_customTextCache) {
      this.updateProgress(_customCacheIndex, _customTextCache.length)
    } else {
      this.updateProgress(_cacheIndex, this.getCachedTextItems(this.getQueueItemId(this.state.queueIndex)).length)
    }
  }
  onplaying = (event) => {
    // log(event)
    const audio = event.target && audioProps(event.target)
    if (audio) {
      // this.updateProgress(audio.currentTime, audio.duration)
      // this.setState({loading: false})
    }
  }
  onprogress = this.onabort
  onratechange = this.onabort
  onseeked = this.onabort
  onseeking = this.onabort
  onstalled = this.onabort
  onsuspend = this.onabort
  ontimeupdate = this.ondurationchange
  onvolumechange = this.onabort
  onwaiting = this.onabort
  cancel = () => {
    const {
      displayText, 
      queue, 
      queueIndex, 
      queueWebsite,
      progressValue, 
      progressMax, 
      progressAdjustedValue, 
      progressPercent,
      selectedQueueItemId
    } = this.initialState
    this.synthCancel()
    this.manualEnd = true
    setTimeout(()=>{
      this.setState({
        ...this.getSynthState(),
        displayText,
        queue,
        queueIndex,
        queueWebsite,
        progressValue,
        progressMax,
        progressAdjustedValue,
        progressPercent,
        selectedQueueItemId
      })
    }, 100)
  }
  play = () => {
    if (_customTextCache)
      if (!this.speakCached(null, _customCacheIndex))
        console.error('error play')
    else
      this.speakQueueItem(this.state.queueIndex)
  }
  play = throttle(this.play, 1000)
  pause = () => {
    this.synthPause(_currAudio)
    this.onpause()
  }
  pause = throttle(this.pause, 1000)
  resume = () => {
    this.synthResume(_currAudio)
    this.onresume()
  }
  resume = throttle(this.resume, 1000)
  isLoaded = () => {
    if (this.getSynthType() === 'responsiveVoice')
      return this.getSynth().isPlaying() || this.state.displayText
    return this.state.loaded
  }
  isEnded = () => this.state.ended
  isSpeaking = () => this.state.speaking
  isPending = () => this.state.pending
  isPaused = () => this.state.paused
  queueEmpty = () => this.state.queue.length === 0
  startOfQueue = () => this.state.queueIndex === 0
  endOfQueue = () => this.state.queueIndex === this.state.queue.length - 1
  handleChange = (event) => {
    this.setState({
      value: event.target.value,
    })
  }
  handleSynthTypeChange = (event, key, synthType) => {
    this.cancel()
    this.setSynthType(synthType)
  }
  handleVoiceChange = (event, key, voice) => {
    this.cancel()
    this.setVoice(voice)
  }
  handleWebsiteChange = (event, key, website) => {
    const prevWebsite = this.state.website
    this.cancel()
    this.resetItems()
    if (prevWebsite)
      firebase
        .database()
        .ref(prevWebsite.key)
        .off('child_added', this.onChildAdded)
    firebase
      .database()
      .ref(website.key)
      .orderByChild('updated_at')
      .limitToLast(LIMIT)
      .on('child_added', this.onChildAdded)
    this.setState({website})
  }
  handleProgressDragStart = () => {
    this.setState({
      progressDragging: true
    })
  }
  handleProgressChange = (event, value) => {
    this.setState({
      progressChangeValue: value
    })
  }
  handleProgressDragStop = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice') {

    } else if (synthType === 'native') {
      const progressValue = getEndWordIndex(this.utterance.text, this.state.progressChangeValue)
      const progressMax = this.utterance.text.length
      const displayText = this.utterance.text.slice(progressValue, progressMax)
      this.speak(displayText, true)
      this.setState({
        ...this.getSynthState(),
        displayText,
        progressChangeValue: this.initialState.progressChangeValue,
        progressDragging: this.initialState.progressDragging,
      })
      this.updateProgress(progressValue, progressMax)
    } else if (synthType === 'watsonSpeech') {
      if (!this.speakCached(this.getQueueItemId(this.state.queueIndex), this.state.progressChangeValue))
        console.error('error slider at ' + this.state.progressChangeValue)
    }
  }
  renderDisplayText = () => {
    const synthType = this.getSynthType()
    let displayText = this.state.displayText
    if (synthType === 'responsiveVoice') {

    } else if (synthType === 'watsonSpeech') {
      if (this.state.selectedQueueItemId)
        displayText = this.getCachedTextItem(this.state.selectedQueueItemId, _cacheIndex)
      else if (this.getCachedTextItems())
        displayText = this.getCachedTextItem(null, _customCacheIndex)
    } 
    else if (synthType === 'native' && displayText && this.state.charIndex > 0) {
      const endWordIndex = getEndWordIndex(displayText, this.state.charIndex)
      displayText = displayText.slice(Math.max(0, endWordIndex-100), endWordIndex)
    }
    return displayText ? 
      <Toolbar className="App-displayText" style={styles.displayText}>
        <ToolbarGroup lastChild={true} className="App-displayTextGroup">
          <ToolbarTitle text={displayText} className="App-displayTextTitle" style={styles.displayTextTitle}/>
        </ToolbarGroup>
      </Toolbar> 
    : null
  }
  renderProgressLabel = () => {
    const synthType = this.getSynthType()
    if (synthType === 'responsiveVoice')
      return <ToolbarTitle 
        className="App-sliderLabel" 
        style={styles.sliderLabel}
        text={(this.state.progressValue + 1) + '/' + this.state.progressMax} />
    else if (synthType === 'watsonSpeech') {
      return <ToolbarTitle 
        className="App-sliderLabel" 
        style={styles.sliderLabel}
        text={(this.state.progressValue + 1) + '/' + this.state.progressMax} />
    } else if (synthType === 'native') {
      return <ToolbarTitle 
        className="App-sliderLabel" 
        style={styles.sliderLabel}
        text={(this.state.progressValue + 1) + '/' + this.state.progressMax} />
    }
    return null
  }
  renderSlider = () => {
    const synthType = this.getSynthType()
    if (synthType === 'native' 
      // || (synthType === 'watsonSpeech' && this.state.progressMax !== 1)
    )
      return <Slider
        className="App-slider"
        sliderStyle={styles.slider}
        min={0}
        max={this.state.progressMax}
        step={1}
        defaultValue={0}
        value={this.state.progressAdjustedValue}
        onChange={this.handleProgressChange}
        onDragStart={this.handleProgressDragStart}
        onDragStop={this.handleProgressDragStop}
      />
    return null
  }
  render() {
    if (!this.voiceSupport()) {
      return <p>Your device does not support {this.getSynthType()}</p>
    }
    return (
      <MuiThemeProvider>
        <div className="App">
          <div className="App-body">
            {this.state.loading ? 
              <div className="App-overlay">
                <CircularProgress />
              </div> 
            : null}
            <div className="App-customTextRow">
              <TextField
                ref={n=>this.textInput=n}
                hintText="Custom text"
                floatingLabelText="Custom text"
                multiLine={true}
                fullWidth={true}
                value={this.state.value}
                onChange={this.handleChange}
              />
              {this.state.value ? 
                <RaisedButton label="Speak" primary={true} onClick={this.speakCustomText} /> :
                null}
            </div>
            <div className="App-settingsRow">
              <SynthTypeSelectField value={this.state.synthType} synthTypes={SYNTH_TYPES} onChange={this.handleSynthTypeChange}/>
              <VoiceSelectField value={this.state.voice} voices={this.state.voices} onChange={this.handleVoiceChange}/>
            </div>
            <div className="App-websiteRow">
              <WebsiteSelectField value={this.state.website} onChange={this.handleWebsiteChange}/>
            </div>
            {this.isLoaded() ? 
              <div className="App-playerControls">
                <Toolbar>
                  <ToolbarGroup firstChild={true} style={styles.toolbarGroup}>
                    <RaisedButton style={styles.toolbarButton} label="Cancel" onClick={this.cancel} />
                  </ToolbarGroup>
                </Toolbar>
                <Toolbar>
                  <ToolbarGroup>
                    {this.renderProgressLabel()}
                  </ToolbarGroup> 
                </Toolbar>
                <Toolbar>
                  <ToolbarGroup style={styles.toolbarGroup} firstChild={true}>
                    {!this.queueEmpty() && !this.startOfQueue() ?
                      <RaisedButton 
                        style={styles.toolbarButton} 
                        label="Prev" 
                        onClick={this.prev} 
                        /> 
                    : null}
                    {this.isLoaded() && this.isPaused() && this.isEnded() ? 
                      <RaisedButton 
                        style={styles.toolbarButton} 
                        label={"Play"} 
                        onClick={this.play} 
                        />
                    : null}
                    {this.isLoaded() && this.isPaused() && !this.isEnded() ? 
                      <RaisedButton 
                        style={styles.toolbarButton} 
                        label={"Resume"} 
                        onClick={this.resume} 
                        />
                    : null}
                    {this.isLoaded() && !this.isPaused() ?
                      <RaisedButton 
                        style={styles.toolbarButton} 
                        label="Pause" 
                        onClick={this.pause} 
                        />
                    : null}
                    {!this.queueEmpty() && !this.endOfQueue() ?
                      <RaisedButton 
                        style={styles.toolbarButton} 
                        label="Next" 
                        onClick={this.next} 
                        /> 
                    : null}
                  </ToolbarGroup>
                </Toolbar>
              </div>
            : null}
            {this.renderDisplayText()}
            {this.state.website ? 
              <PlayList
                items={this.getItems()}
                selected={this.state.selectedQueueItemId}
                renderRow={(item, index) => (
                  <this.state.website.ListItem
                    key={item[this.state.website.uniqKey]} 
                    selected={this.state.selectedQueueItemId} 
                    item={item}
                    onClick={() => this.speakItem(item, index)}
                    />
                )}
                />
            : null}
            {this.state.website ?
              <div className="App-buttonRow">
                <RaisedButton label="Load more" style={styles.loadButton} onClick={this.loadMore} />
              </div>
            : null}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
